# Go-usvc

Go based Product API built using the Gorilla Toolkit [https://www.gorillatoolkit.org/](https://www.gorillatoolkit.org/)


## Documentation

OpenAPI documentation can be found in the [swagger.yaml](../../api/swagger.yaml) file

## Running

The application can be run with `go run main.go`

```
➜ go run main.go
go-usvc 2020/11/12 16:57:05 Starting server on port 9090

curl localhost:9090/products
```