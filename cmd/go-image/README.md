# Go-image

## Uploading 

Note: need to use `--data-binary` to ensure file is not converted to text

```
curl -vv localhost:9091/1/go.mod -X PUT --data-binary @test.png
```